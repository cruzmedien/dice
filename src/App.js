import React, { Component } from "react";
import RollDie from "./RollDice";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <RollDie />
      </div>
    );
  }
}

export default App;
